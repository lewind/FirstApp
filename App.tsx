/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */
import 'react-native-gesture-handler';
import React from 'react';
import { StatusBar, SafeAreaView } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { MainTabNavigation } from './src/navigation/MainTabNavigation';

const App = () => {
  return (
    <NavigationContainer>
      <StatusBar barStyle="dark-content"/>
      <SafeAreaView style={{flex: 1}}>
        <MainTabNavigation />
      </SafeAreaView>
    </NavigationContainer>
    
  );
};

export default App;