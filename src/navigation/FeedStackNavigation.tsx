import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { Feed } from '../views/feeds/Feed';
import { FeedDetail } from '../views/feeds/FeedDetail';

const Stack = createStackNavigator();

export const FeedStackNavigation = ()=>(
    <Stack.Navigator>
        <Stack.Screen name="Feed" component={Feed} options={{title: "Tin"}}/>
        <Stack.Screen name="FeedDetail" component={FeedDetail} options={{title: "Chi tiết"}}/>
    </Stack.Navigator>
)