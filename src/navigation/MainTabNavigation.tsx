import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { Feed } from '../views/feeds/Feed';
import { Notification } from '../views/notifications/Notification';
import { Profile } from '../views/profile/Profile';
import { FeedStackNavigation } from './FeedStackNavigation';

const Tab = createBottomTabNavigator();

export const MainTabNavigation = () => (
    <Tab.Navigator>
        <Tab.Screen name="FeedStack" component={FeedStackNavigation} options={{ title: "Trang chủ" }} />
        <Tab.Screen name="Notification" component={Notification} options={{ title: "Thông báo" }} />
        <Tab.Screen name="Profile" component={Profile} options={{ title: "Cá nhân" }} />
    </Tab.Navigator>
)