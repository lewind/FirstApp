import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

export const Profile = () => {
    return (
        <View style={styles.container}>
            <Text style={{fontSize: 30}}>Profile</Text>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
    }
})