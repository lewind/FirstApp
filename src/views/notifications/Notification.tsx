import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

export const Notification = () => {
    return (
        <View style={styles.container}>
            <Text style={{fontSize: 30}}>Notification</Text>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
    }
})