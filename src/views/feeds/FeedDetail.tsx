import { useRoute } from '@react-navigation/core';
import React from 'react';
import { Button, StyleSheet, Text, TouchableOpacity, View } from 'react-native';

export const FeedDetail = () => {
    const route = useRoute();
    return (
        <View style={styles.container}>
            <Text style={{fontSize: 30}}>{route.params.hello}</Text>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
    }
})