import { useNavigation } from '@react-navigation/core';
import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';

export const Feed = () => {
    const navigation = useNavigation();
    return (
        <View style={styles.container}>
            <Text style={{fontSize: 30}}>Feeds</Text>
            <TouchableOpacity onPress={()=>{
                navigation.navigate("FeedDetail", {hello: "Hello World"});
            }}>
                <Text>Click me</Text>
            </TouchableOpacity>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
    }
})